close all; 
clear; 
clc;


physical_constants;
unit = 1e-3; # millimeters


f_start = 325e6;
f0 = 435e6; # center frequency
f_stop = 500e6;
lambda = c0 / f0 * 1000; # wavelength in mm
ratio = 0.48*lambda;
dipole.halfwidth  = 0.5;  # half-width of dipole metal
dipole.leglength = ((0.44*lambda)/2); # length of ONE dipole leg
disp( ['Dipole length  = ' num2str(2*dipole.leglength) ' mm']);


FDTD = InitFDTD('NrTS',  120000 );
FDTD = SetGaussExcite( FDTD, f0, f0/3 );
BC = {'MUR' 'MUR' 'MUR' 'MUR' 'MUR' 'MUR'}; % boundary 
%BC = {'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8'}; % better EM field absorbtion 
FDTD = SetBoundaryCond( FDTD, BC );

# CSXCAD & mesh
CSX = InitCSX();
SimBox = [lambda lambda lambda];
x_mesh = [-600:20:-151 , -151, -149:10:-5, -6, -4, 0, 4, 6:10:151, 149, 151:20:600];
y_mesh = [-600:20:-5, -10, -5, -4, -3, -2, 0, 2, 3, 4, 5, 10, 20:20:600];
z_mesh = [[-600:20:0, 0.1 , 10:20:600]];
mesh.x = x_mesh;
mesh.y = y_mesh;
mesh.z = z_mesh;


%antenna right beam
CSX = AddMetal(CSX, 'Qubik-antenna_right_dipole'); 
CSX = ImportSTL(CSX, 'Qubik-antenna_right_dipole',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-antenna_right_dipole.stl');

%antenna left beam
CSX = AddMetal(CSX, 'Qubik-antenna_left_dipole'); 
CSX = ImportSTL(CSX, 'Qubik-antenna_left_dipole',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-antenna_left_dipole.stl');

%antenna right mount
CSX = AddMaterial( CSX, 'Qubik-antenna_right_mount');
CSX = SetMaterialProperty(CSX, 'Qubik-antenna_right_mount', 'Epsilon',3.9,'Mue',1);
CSX = ImportSTL(CSX, 'Qubik-antenna_right_mount',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-antenna_right_mount.stl');

%antenna left mount
CSX = AddMaterial(CSX, 'Qubik-antenna_left_mount');
 CSX = SetMaterialProperty(CSX, 'Qubik-antenna_left_mount', 'Epsilon',3.9,'Mue',1);
CSX = ImportSTL(CSX, 'Qubik-antenna_left_mount',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-antenna_left_mount.stl');

%antenna base plate
CSX = AddMaterial(CSX, 'Qubik-base_plate');
CSX = SetMaterialProperty(CSX, 'Qubik-base_plate', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-base_plate',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-base_plate.stl');

%antenna solar power1
CSX = AddMaterial(CSX, 'Qubik-solar_power_1');
CSX = SetMaterialProperty(CSX, 'Qubik-solar_power_1', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-solar_power_1',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-solar_power_1.stl');

%antenna solar power2
CSX = AddMaterial(CSX, 'Qubik-solar_power_2');
CSX = SetMaterialProperty(CSX, 'Qubik-solar_power_2', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-solar_power_2',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-solar_power_2.stl');

%antenna solar power3
CSX = AddMaterial(CSX, 'Qubik-solar_power_3');
CSX = SetMaterialProperty(CSX, 'Qubik-solar_power_3', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-solar_power_3',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-solar_power_3.stl');

%antenna solar power4
CSX = AddMaterial(CSX, 'Qubik-solar_power_4');
CSX = SetMaterialProperty(CSX, 'Qubik-solar_power_4', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-solar_power_4',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-solar_power_4.stl');

%antenna solar power top
CSX = AddMaterial(CSX, 'Qubik-solar_power_top');
CSX = SetMaterialProperty(CSX, 'Qubik-solar_power_top', 'Epsilon', 3.9, 'Mue', 1);
CSX = ImportSTL(CSX, 'Qubik-solar_power_top',1,'/home/aris/Documents/project/openems/openems-lsf/FreeCAD_model/Qubik-solar_power_top.stl');




# dipole feed point
feed.R = 50; # feed point impedance
start = [-5 -3.95 0.1];
stop =  [ 5  3.95 0];
[CSX port] = AddLumpedPort(CSX, 1, 1, feed.R, start, stop, [1 0 0], true);


# generate a smooth mesh with max. cell size: lambda_min / 20
max_res = c0 / f0 / unit / 50;
CSX = DefineRectGrid(CSX, unit, mesh);

%Add a nf2ffbox
SimBox = SimBox - max_res * 4; %reduced SimBox size for nf2ff box
[CSX nf2ff] = CreateNF2FFBox(CSX, 'nf2ff', -SimBox/2, SimBox/2);

# simulation
Sim_Path = 'tmp_Qubik';
Sim_CSX = 'Qubik.xml';
try confirm_recursive_rmdir(false, 'local'); end
[status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous
[status, message, messageid] = mkdir( Sim_Path ); % create empty folder
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX ); % save file
CSXGeomPlot( [Sim_Path '/' Sim_CSX] ); % display geometry
RunOpenEMS( Sim_Path, Sim_CSX, ' -vvv');

# calculate Zin
freq = linspace( f_start, f_stop, 501 );
port = calcPort(port, Sim_Path, freq);
Zin = port.uf.tot ./ port.if.tot;
s11 = port.uf.ref ./ port.uf.inc;
P_in = 0.5 * port.uf.inc .* conj( port.if.inc ); % antenna feed power


%% Smith chart port reflection
plotRefl(port, 'threshold', -10)
title( 'reflection coefficient' );


% plot feed point impedance
figure
plot( freq/1e6, real(Zin), 'k-', 'Linewidth', 2 );
hold on
grid on
plot( freq/1e6, imag(Zin), 'r--', 'Linewidth', 2 );
title( 'feed point impedance' );
xlabel( 'frequency f / MHz' );
ylabel( 'impedance Z_{in} / Ohm' );
legend( 'real', 'imag' );


% plot reflection coefficient S11
figure
plot( freq/1e6, 20*log10(abs(s11)), 'k-', 'Linewidth', 2 );
grid on
title( 'reflection coefficient S_{11}' );
xlabel( 'frequency f / MHz' );
ylabel( 'reflection coefficient |S_{11}|' );
 
drawnow

%% NFFF contour plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%find resonance frequency from s11
f_res_ind = find(s11==min(s11));
f_res = freq(f_res_ind);
 
% calculate the far field at phi=0 degrees and at phi=90 degrees
disp( 'calculating far field at phi=[0 90] deg...' );
 
nf2ff = CalcNF2FF(nf2ff, Sim_Path, f_res, [-180:2:180]*pi/180, [0 90]*pi/180);
 
% display power and directivity
disp( ['radiated power: Prad = ' num2str(nf2ff.Prad) ' Watt']);
disp( ['directivity: Dmax = ' num2str(nf2ff.Dmax) ' (' num2str(10*log10(nf2ff.Dmax)) ' dBi)'] );
disp( ['efficiency: nu_rad = ' num2str(100*nf2ff.Prad./real(P_in(f_res_ind))) ' %']);
 
% normalized directivity as polar plot
figure
polarFF(nf2ff,'xaxis','theta','param',[1 2],'normalize',1)
 
% log-scale directivity plot
figure
plotFFdB(nf2ff,'xaxis','theta','param',[1 2])
% conventional plot approach
% plot( nf2ff.theta*180/pi, 20*log10(nf2ff.E_norm{1}/max(nf2ff.E_norm{1}(:)))+10*log10(nf2ff.Dmax));
 
drawnow
 
%%
disp( 'calculating 3D far field pattern and dumping to vtk (use Paraview to visualize)...' );
thetaRange = (0:2:180);
phiRange = (0:2:360) - 180;
nf2ff = CalcNF2FF(nf2ff, Sim_Path, f_res, thetaRange*pi/180, phiRange*pi/180,'Verbose',1,'Outfile','3D_Pattern.h5');
 
figure
plotFF3D(nf2ff,'logscale',-20);
 
 
E_far_normalized = nf2ff.E_norm{1} / max(nf2ff.E_norm{1}(:)) * nf2ff.Dmax;
DumpFF2VTK([Sim_Path '/3D_Pattern.vtk'],E_far_normalized,thetaRange,phiRange,'scale',1e-3);
